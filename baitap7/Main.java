package baitap7;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws ParseException {
        Scanner scan = new Scanner(System.in);
        int chon2;
        ArrayList<KhachHang> list = new ArrayList<KhachHang>();
        while (true){
            System.out.println("Menu chuc nang");
            System.out.println("1. Nhap thong tin khach hang.");
            System.out.println("2. In danh sach khach hang va thanh tien.");
            System.out.println("3. Xuat ra so luong KH VietNam, nuoc ngoai");
            System.out.println("4. In ra trung binh thanh tien cua KH nuoc ngoai");
            System.out.println("5. In ra hoa don trong thang 9 nam 2013");
            System.out.println("6. Thoat\nMoi chon chuc nang"); int chon1 = scan.nextInt();
            switch (chon1){
                case 1:
                    do{
                        System.out.println("Khach hang la nguoi Vietnam hay nuoc ngoai? 1 - Vietnam, 2 - Nuoc ngoai");
                        int chon3 = scan.nextInt();
                        switch (chon3){
                            case 1:
                                KhachHangVietNam khv = new KhachHangVietNam();
                                khv.nhap();
                                list.add(khv);
                                break;
                            case 2:
                                KhachHangNuocNgoai khn = new KhachHangNuocNgoai();
                                khn.nhap();
                                list.add(khn);
                                break;
                        }
                        System.out.println("Ban co muon nhap tiep ko? 1 - co, 2 - khong"); chon2 = scan.nextInt();
                    }while (chon2 == 1);
                    continue;
                case 2:
                    for(KhachHang kh : list){
                        if(kh instanceof KhachHangVietNam){
                            KhachHangVietNam khv = new KhachHangVietNam();
                            khv = (KhachHangVietNam) kh;
                            khv.xuat();
                        }
                        else {
                            KhachHangNuocNgoai khn = new KhachHangNuocNgoai();
                            khn = (KhachHangNuocNgoai) kh;
                            khn.xuat();
                        }
                    }
                    continue;
                case 3:
                    int sumKHV = 0, sumKHN = 0;
                    for(KhachHang kh : list){
                        if(kh instanceof KhachHangVietNam)
                            sumKHV++;
                        else
                            sumKHN++;
                    }
                    System.out.println("Co "+sumKHV+" khach hang vietnam va "+sumKHN+" khach hang nuoc ngoai");
                    continue;
                case 4:
                    double sum = 0;
                    int i=0;
                    for(KhachHang kh : list)
                        if(kh instanceof KhachHangNuocNgoai){
                            i++;
                            KhachHangNuocNgoai khn = new KhachHangNuocNgoai();
                            sum += khn.getThanhTien();
                        }
                    if(i==0)
                        System.out.println("Khong co khach hang nuoc ngoai");
                    else
                        System.out.println("Thanh tien trung binh cua KH nuoc ngoai la: "+(sum/i));
                    continue;
                case 5:
                    break;

            }
            break;

        }

    }
}
