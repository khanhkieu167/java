package baitap7;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class KhachHang {
    String maKhachHang;
    String hoTen;
    Date ngay;
    int soLuong;
    int donGia;

    public String getMaKhachHang() {
        return maKhachHang;
    }

    public void setMaKhachHang(String maKhachHang) {
        this.maKhachHang = maKhachHang;
    }

    public String getHoTen() {
        return hoTen;
    }

    public void setHoTen(String hoTen) {
        this.hoTen = hoTen;
    }

    public Date getNgay() {
        return ngay;
    }

    public void setNgay(String ngay) throws ParseException {
        SimpleDateFormat fm = new SimpleDateFormat("dd MM yyyy");
        this.ngay = fm.parse(ngay);
    }

    public int getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(int soLuong) {
        this.soLuong = soLuong;
    }

    public int getDonGia() {
        return donGia;
    }

    public void setDonGia(int donGia) {
        this.donGia = donGia;
    }

    public void nhap() throws ParseException {
        Scanner scan = new Scanner(System.in);
        System.out.println("Nhap vao thong tin khach hang");
        System.out.println("Nhap vao ma khach hang: "); setMaKhachHang(scan.nextLine());
        System.out.println("Nhap vao ten khach hang: "); setHoTen(scan.nextLine());
        System.out.println("Nhap vao ra hoa don theo dang dd MM yyyy: "); setNgay(scan.nextLine());
        System.out.println("Nhap vao so luong: "); setSoLuong(scan.nextInt());
        System.out.println("Nhap vao don gia: "); setDonGia(scan.nextInt());
    }
    public void xuat() throws ParseException {
        System.out.println("Ma khach hang: "+getMaKhachHang());
        System.out.println("Khach hang: "+getHoTen());

        System.out.println("Ngay ra hoa don: "+getNgay());
        System.out.println("So luong: "+getSoLuong());
        System.out.println("Don gia: "+getDonGia());
    }
}
