package baitap7;

import java.text.ParseException;
import java.util.Scanner;

public class KhachHangNuocNgoai extends KhachHang {
    String quocTich;
    double thanhTien;

    public double getThanhTien() {
        return thanhTien;
    }

    public void setThanhTien() {
        this.thanhTien = this.soLuong * this.donGia;
    }

    public String getQuocTich() {
        return quocTich;
    }

    public void setQuocTich(String quocTich) {
        this.quocTich = quocTich;
    }
    public void nhap() throws ParseException {
        Scanner scan = new Scanner(System.in);
        super.nhap();
        System.out.println("Nhap vao quoc tich: "); setQuocTich(scan.nextLine());
    }
    public void xuat() throws ParseException {
        super.xuat();
        System.out.println("Quoc tich: "+getQuocTich());
        setThanhTien();
        System.out.println("Thanh tien: "+getThanhTien());
    }
}
