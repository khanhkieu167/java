package baitap7;

import java.text.ParseException;
import java.util.Scanner;

public class KhachHangVietNam extends KhachHang {
    String doiTuong;
    int dinhMuc;
    double thanhTien;

    public String getDoiTuong() {
        return doiTuong;
    }

    public void setDoiTuong(String doiTuong) {
        this.doiTuong = doiTuong;
    }

    public int getDinhMuc() {
        return dinhMuc;
    }

    public void setDinhMuc(int dinhMuc) {
        this.dinhMuc = dinhMuc;
    }

    public double getThanhTien() {
        return thanhTien;
    }

    public void setThanhTien() {
        if(this.soLuong <= this.dinhMuc)
            this.thanhTien = this.soLuong * this.donGia;
        else
            this.thanhTien = this.dinhMuc * this.donGia + (this.soLuong - this.dinhMuc)*this.donGia*2.5;
    }

    public void nhap() throws ParseException {
        Scanner scan = new Scanner(System.in);
        super.nhap();
        System.out.println("Khach hang thuoc doi tuong nao?"); setDoiTuong(scan.nextLine());
        System.out.println("Dinh muc cua khach hang: "); setDinhMuc(scan.nextInt());
    }
    public void xuat() throws ParseException {
        super.xuat();
        System.out.println("Doi tuong: "+getDoiTuong());
        System.out.println("Dinh muc: "+getDinhMuc());
        setThanhTien();
        System.out.println("Thanh tien: "+getThanhTien());

    }
}
