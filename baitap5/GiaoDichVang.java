package baitap5;

import java.text.ParseException;
import java.util.Scanner;

public class GiaoDichVang extends GiaoDich {
    String loaiVang;
    long thanhTien;

    public long getThanhTien() {
        return thanhTien;
    }

    public void setThanhTien() {
        this.thanhTien = this.soLuong*this.donGia;
    }

    public String getLoaiVang() {
        return loaiVang;
    }

    public void setLoaiVang(String loaiVang) {
        this.loaiVang = loaiVang;
    }

    public void nhap(){
        Scanner scan = new Scanner(System.in);
        super.nhap();
        System.out.println("Nhap vao loai vang: "); setLoaiVang(scan.nextLine());
    }

    public void xuat() throws ParseException {
        super.xuat();
        System.out.println("\tLoai vang: "+getLoaiVang());
        setThanhTien();
        System.out.println("\tThanh tien: "+getThanhTien());
    }
}
