package baitap5;

import java.text.ParseException;
import java.util.Scanner;

public class GiaoDichTien extends GiaoDich {
    long tiGia;
    String loaiTien;
    long thanhTien;

    public long getTiGia() {
        return tiGia;
    }

    public void setTiGia(long tiGia) {
        this.tiGia = tiGia;
    }

    public String getLoaiTien() {
        return loaiTien;
    }

    public void setLoaiTien(String loaiTien) {
        this.loaiTien = loaiTien;
    }

    public long getThanhTien() {
        return thanhTien;
    }

    public void setThanhTien() {
        if( getLoaiTien().equals("usd") || getLoaiTien().equals("euro"))
            this.thanhTien = this.donGia*this.soLuong*this.tiGia;
        else
            this.thanhTien = this.soLuong*this.donGia;
    }
    public void nhap(){
        Scanner scan = new Scanner(System.in);
        super.nhap();
        System.out.println("Nhap vao ti gia: "); setTiGia(scan.nextLong());
        scan.nextLine();
        System.out.println("Nhap vao loai tien (usd, euro, vnd): "); setLoaiTien(scan.nextLine());
    }
    public void xuat() throws ParseException {
        super.xuat();
        setThanhTien();
        System.out.println("\tLoai tien: "+getLoaiTien());
        System.out.println("\tTi gia: "+getTiGia());
        System.out.println("\tThanh tien: "+getThanhTien());
    }
}
