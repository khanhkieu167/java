package baitap5;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws ParseException {
        int chon2;
        Scanner scan = new Scanner(System.in);
        ArrayList<GiaoDich> list = new ArrayList<GiaoDich>();
        while (true){
            System.out.println();
            System.out.println("Danh sach chuc nang");
            System.out.println("1. Nhap danh sach cac giao dich");
            System.out.println("2. Hien thi danh sach cac giao dich");
            System.out.println("3. Tinh tong so luong tung loai giao dich");
            System.out.println("4. Tinh trung binh thanh tien cua giao dich tien te");
            System.out.println("5. Xuat ra cac giao dich co don gia > 1 ty vnd");
            System.out.println("6. Thoat");
            System.out.println("Moi ban chon chuc nang");
            int chon = scan.nextInt();
            switch (chon){
                case 1:
                    do{
                        System.out.println("Chon loai giao dich: 1 - vang, 2 - tien te");
                        int chon1 = scan.nextInt();
                        switch (chon1){
                            case 1:
                                GiaoDichVang gdv = new GiaoDichVang();
                                gdv.nhap();
                                list.add(gdv);
                                break;
                            case 2:
                                GiaoDichTien gdt = new GiaoDichTien();
                                gdt.nhap();
                                list.add(gdt);
                                break;
                        }
                        System.out.println("Ban co muon chon nua ko? 1-co 2-khong");
                        chon2 = scan.nextInt();
                    }while (chon2 == 1);
                    continue;
                case 2:
                    int i = 1;
                    for(GiaoDich gd : list){
                        if(gd instanceof GiaoDichVang){
                            System.out.println("Giao dich thu "+i+": giao dich vang"); i++;
                            GiaoDichVang gdv = new GiaoDichVang();
                            gdv = (GiaoDichVang) gd;
                            gdv.xuat();
                        }
                        else {
                            System.out.println("Giao dich thu "+i+": giao dich tien"); i++;
                            GiaoDichTien gdt = new GiaoDichTien();
                            gdt = (GiaoDichTien) gd;
                            gdt.xuat();
                        }
                    }
                    continue;
                case 3:
                    int slgdv = 0, slgdt = 0;
                    for(GiaoDich gd : list){
                        if(gd instanceof GiaoDichVang)
                            slgdv++;
                        else
                            slgdt ++;
                    }
                    System.out.println("Co "+slgdv+" giao dich vang va "+slgdt+" giao dich tien");
                    continue;
                case 4:
                    long thanhTien = 0; i = 0;
                    for(GiaoDich gd : list){
                        if(gd instanceof GiaoDichTien){
                            i++;
                            GiaoDichTien gdt = new GiaoDichTien();
                            gdt = (GiaoDichTien) gd;
                            thanhTien += gdt.getThanhTien();
                        }
                    }
                    if(i==0)
                        System.out.println("Khong co giao dich tien");
                    else
                        System.out.println("Thanh tien trung binh cua cac giao dich tien la: "+(thanhTien/i));
                    continue;
                case 5:
                    System.out.println("Xuat ra cac giao dich co don gia > 1 ty vnd");
                    for(GiaoDich gd : list){
                        if(gd instanceof GiaoDichVang){
                            GiaoDichVang gdv = new GiaoDichVang();
                            gdv = (GiaoDichVang) gd;
                            if(gdv.getDonGia() > 1000000000)
                                gdv.xuat();
                        }
                        else {
                            GiaoDichTien gdt = new GiaoDichTien();
                            gdt = (GiaoDichTien) gd;
                            if(gdt.getDonGia() > 1000000000)
                                gdt.xuat();
                        }
                    }
                    continue;
                case 6:
                    break;
            }
            break;
        }
    }
}
