package baitap5;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class GiaoDich {
    String maGiaoDich;
    String ngayGiaoDich;
    long donGia;
    int soLuong;

    public String getMaGiaoDich() {
        return maGiaoDich;
    }

    public void setMaGiaoDich(String maGiaoDich) {
        this.maGiaoDich = maGiaoDich;
    }

    public String getNgayGiaoDich() {
        return ngayGiaoDich;
    }

    public void setNgayGiaoDich(String ngayGiaoDich) {
        this.ngayGiaoDich = ngayGiaoDich;
    }

    public long getDonGia() {
        return donGia;
    }

    public void setDonGia(long donGia) {
        this.donGia = donGia;
    }

    public int getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(int soLuong) {
        this.soLuong = soLuong;
    }

    public void nhap(){
        Scanner scan = new Scanner(System.in);
        System.out.println("Nhap vao ma giao dich"); setMaGiaoDich(scan.nextLine());
        System.out.println("Nhap vao ngay giao dich theo dang dd MM yyyy"); setNgayGiaoDich(scan.nextLine());
        System.out.println("Nhap vao don gia"); setDonGia(scan.nextLong());
        System.out.println("Nhap vao so luong"); setSoLuong(scan.nextInt());
    }

    public void xuat() throws ParseException {
        System.out.println("\tMa giao dich: "+getMaGiaoDich());
        SimpleDateFormat fm = new SimpleDateFormat("dd MM yyyy");
        Date date = fm.parse(getNgayGiaoDich());
        System.out.println("\tNgay giao dich: "+date);
        System.out.println("\tDon gia: "+getDonGia());
        System.out.println("\tSo luong: "+getSoLuong());
    }
}
